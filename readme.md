#### global
window object in browser or global context in workorder

    window.global == window == global // true
    
#### _defineProperty
define a function or a property in a object context that is not writable

    _defineProperty( global , 'myglobalproperty' , 'myglobalvalue' );
    
    _defineProperty( myobject , 'myfunction' , function(){...} );

#### isset
    isset( whatyouwant ); // return true / false
    
    isset( global ); // return true
    
    isset( global ); // return true

#### isnull
    isnull( whatyouwant ); // return true / false
    
    isnull( global ); // return false
    
    isnull( undefined ); // return false
    
    isnull( null ); // return true

#### exist
return true if the argument is not undefined and not null

    exist( whatyouwant ); // return true / false
    
    exist( global ); // return true
    
    isnull( undefined ); // return false
    
    isnull( null ); // return false

#### isdate
    isdate( whatyouwant ); // return true / false
    
    isdate( Date.now() ); // return false
    
    isnull( new Date( NaN ) ); // return true

#### isarray
    isarray( whatyouwant ); // return true / false
    
    isarray( [] ); // return true
    
    isarray( new Array() ); // return true
    
    isarray( {} ); // return false

#### isnodelist
    isnodelist( whatyouwant ); // return true / false
    
    isnodelist( [] ); // return false
    
    isnodelist( document.getElementsByClassName(...) ); // return true

#### isobject
    isobject( whatyouwant ); // return true / false
    
    isobject( [] ); // return false
    
    isobject( new Array() ); // return false
    
    isobject( {} ); // return true

#### isEmptyObject
    isEmptyObject( whatyouwant ); // return true / false
    
    isEmptyObject( [] ); // return false
    
    isEmptyObject( { a : 'b' } ); // return false
    
    isEmptyObject( {} ); // return true

#### isstring
    isstring( whatyouwant ); // return true / false
    
    isstring( "" ); // return true
    
    isstring( new String() ); // return true
    
    isstring( {} ); // return false

#### isfillstring
    isfillstring( whatyouwant ); // return true / false
    
    isfillstring( "   " ); // return false
    
    isfillstring( "not empty" ); // return true

#### isboolean
    isboolean( whatyouwant ); // return true / false
    
    isboolean( "   " ); // return false
    
    isboolean( true / false ); // return true

#### isinteger
    isinteger( whatyouwant ); // return true / false
    
    isinteger( "   " ); // return false
    
    isinteger( 42 ); // return true
    
    isinteger( 4.2 ); // return false

#### isfloat
    isfloat( whatyouwant ); // return true / false
    
    isfloat( "   " ); // return false
    
    isfloat( 42 ); // return true
    
    isfloat( 4.2 ); // return true

#### isfunction
    isfunction( whatyouwant ); // return true / false
    
    isfunction( "   " ); // return false
    
    isfloat( function(){...} ); // return true

#### isregexp
    isregexp( whatyouwant ); // return true / false
    
    isregexp( "   " ); // return false
    
    isregexp( /.../gi ); // return true
    
    isregexp( new RegExp(...) ); // return true

#### isnode
    isnode( whatyouwant ); // return true / false
    
    isnode( window ); // return false
    
    isnode( document ); // return false
    
    isnode( document.getElementById(...) ); // return true

#### isdocument
    isdocument( whatyouwant ); // return true / false
    
    isdocument( window ); // return false
    
    isdocument( document ); // return true

#### iswindow
    iswindow( whatyouwant ); // return true / false
    
    iswindow( window ); // return true
    
    iswindow( document ); // return false

#### getdocument
    getdocument( whatyouwant ); // return Document || null
    
    getdocument( window ); // return Document
    
    getdocument( document ); // return Document
    
    getdocument( node ); // return Document
    
    getdocument( null ); // return null

#### getdocument
    getwindow( whatyouwant ); // return Window || null
    
    getwindow( window ); // return Window
    
    getwindow( document ); // return Window
    
    getwindow( node ); // return Window
    
    getwindow( null ); // return null

#### domLoad
call the function in argument when or if the DOM is ready

    domLoad( function(){...} );

#### matchesSelector
node.matchesSelector polyfill

    matchesSelector( node , 'selector' ); // true / false

#### closest
    closest( node , 'selector' ); // node || null

#### nl2br
replace \n by "\<br/>" and \t by "\&nbsp;\&nbsp;\&nbsp;\&nbsp;"

    nl2br( string ); // string

#### inarray
    inarray( something , array ); // true / false

#### clonearray
    clonearray( array ); // other cloned array

#### arrayunique
    arrayunique( [ 1 , 1 , 2 , 2 ] ); // [ 1 , 2 ]

#### typedtoarray
    typedtoarray( arguments ); // [...]
    
    typedtoarray( document.getElementsByClassName(...) ); // [...]

#### jsonparse
JSON.parse improved

    jsonparse( string ); // {} || null

#### setImmediate
setImmediate IE/EDGE polyfill

    var id = setImmediate( function(){...} );
    
    clearImmediate( id );

#### getstyleproperty

    getstyleproperty( node , 'inlineproperty' ); // value || null
    
    getstyleproperty( node , 'globalproperty' ); // value || null

#### resetregexp
reset index and lastindex RegExp

    resetregexp( //gi ); // RegExp

#### regexpEscapeString
escape RegExp operator

    regexpEscapeString( string ); // string

#### htmlEscapeString
escape HTML tag

    htmlEscapeString( string ); // string

#### requireJS
load javascript file

    requireJS( src , function(){...} );

#### requireCSS
load stylesheet file

    requireCSS( href , function(){...} );

#### htmlDOM
create HTML elements from string

    htmlDOM( '<p>' ); // <p> element

#### forin
improved for...in

    forin( object , function( key , value ){
    
    } );

#### foreach
improved array.forEach

    forin( array , function( value , index ){
    
    } );

#### counterCallback

    var caller = counterCallback( 2 , function(){
    
        alert('ready');
    
    } );
    
    caller();
    
    caller(); // do alert